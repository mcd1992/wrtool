# wrtool

Rust program meant for OpenWRT based devices to emulate a Netool.io compatible API server.

## Notes
- https://blog.rust-lang.org/2016/05/13/rustup.html
- https://github.com/japaric/rust-cross#cross-compiling-with-cargo

```yaml
# ~/.cargo/config
linker = "$HOME/x-tools/mipsel-unknown-linux-musl/bin/mipsel-unknown-linux-musl-ld"
#ar = "$HOME/x-tools/mipsel-unknown-linux-musl/bin/mipsel-unknown-linux-musl-ar"
rustflags = [
  "-L", "$HOME/x-tools/mipsel-unknown-linux-musl/lib/gcc/mipsel-unknown-linux-musl/8.3.0",
  "-C", "link-args=-I /lib/ld-musl-mipsel-sf.so.1",
  "-C", "target-feature=+crt-static",
]
```
